.PHONY: build start stop rebuild

build:
        @docker build -t devops/dotnetcore-example .

start:
        @docker run -d -p 8081:80 --name dotnetcore-example devops/dotnetcore-example

stop:
        @docker stop -t 0 dotnetcore-example || echo "Container not started"
        @docker container rm -f dotnetcore-example || echo "Container not exists"


rebuild: stop build
